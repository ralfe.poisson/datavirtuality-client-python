from lib.vdal_client import VDALclient

# Configuration
host = "10.29.158.116"
port = "31000"
database = "datavirtuality"
username = "dv_test"
password = "dv_test"
driver_class = "com.datavirtuality.dv.jdbc.Driver"
driver_file = "D:\datavirtuality\jdbc\datavirtuality-jdbc.jar"

# Instantiate client
client = VDALclient(host, port, username, password, driver_file)

# Test: ts_read() from AWB
sql = "select * from ts_read('/NC/DE.HF.ALPIQ/price', '2021-11-23','2021-11-23','')"
print(sql)
result = client.proxy("AWB", sql)
print(result)
