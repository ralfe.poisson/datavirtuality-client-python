import jaydebeapi
import inspect
import datetime

class VDALclient:
    """
    V-DAL Client

    The V-DAL Client class provides a convenient interface for using the DataVirutality service as an SQL Proxy.

    Parameters:
        host (string): The IP address or hostname of the DataVirtuality server
        port (number): The port on which to connect to the DataVirtuality server (default 31000)
        database (string): The name of the database to use (default "datavirtuality")
        use_ssl (boolean): Whether or not to use SSL for connecting to the server (default True)
        user (string): User name for the DataVirtuality user
        password (string): Password for the DataVirtuality user
        driver_class (string): The Java class for the DataVirtuality driver
        driver_file (string): Absolute path of the datavirtuality-jdbc.jar file (including the filename)
        con (object): The database connection pointer for executing SQL statements
    """
        
    host = ""
    port = 31000
    database = "datavirtuality"
    use_ssl = False
    user = ""
    password = ""
    driver_class = "com.datavirtuality.dv.jdbc.Driver"
    driver_file = "D:\datavirtuality\jdbc\datavirtuality-jdbc.jar"
    con = False

    def __init__(self, host, port, user, password, driver_file):
        """
        Constructor for the V-DAL Client

        Parameters:
            host (string): The IP address or host name of the DataVirtuality server. e.g.: "10.29.158.116"
            port (number): The Port on which to connect to the DataVirtuality server. e.g.: "31000"
            user (string): DataVirtuality username
            password (string): Password for the DataVirutality user
            driver_file (string): Absolute path of the datavirtuality-jdbc.jar file (including the file name)

        Returns:
            null
        """

        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.driver_file = driver_file
    
    def connect(self):
        """
        Establish a JDBC connection to the DataVirutality Server

        Returns:
            null
        """

        # Build connection string
        connection_string = ""
        if (self.use_ssl):
            connection_string="jdbc:datavirtuality:{}@mms://{}:{}".format(self.database, self.host, self.port)
        else:
            connection_string="jdbc:datavirtuality:{}@mm://{}:{}".format(self.database, self.host, self.port)

        # Establish JDBC Connection
        self.con = jaydebeapi.connect(self.driver_class, connection_string, [self.user, self.password], self.driver_file,)

    def check_connection(self):
        """
        Check that a connection to DataVirtuality has been made. If not, connect.
        """

        if self.con == False:
            self.connect()

    def query(self, sql):
        """
        Query DataVirtuality using the DataVirtuality-specific SQL dialect

        Parameters:
            sql (string): The SQL query to be executed by DataVirtuality server

        Returns:
            results (Array[object]): An array of records
        """

        self.check_connection()
        cur = self.con.cursor()
        cur.execute(sql)
        results = cur.fetchall() 
        return results
        
    def proxy(self, host, raw_sql):
        """
        Proxy an SQL query through the V-DAL

        This allows for the use of the SQL dialect which is native to the data source, rather than using the DataVirtuality SQL dialect.

        Parameters:
            host (string): The data source configured in DataVirtuality. e.g.: "AWB"
            raw_sql (string): The SQL to execute directly on the data source. e.g.: "SELECT * FROM my_table;"

        Returns:
            result Array(Array())
        """

        sql = self.prepareNativeProcedureCall(host, raw_sql)
        result = self.query(sql)
        return self.filterNoneFromResult(result)

    def prepareNativeProcedureCall(self, host, raw_sql):
        """
        Wrap the SQL to be proxied in the .native() DataVirtuality function.

        Note: As we are not deriving the result structure from an object, we default all columns to string and will then try to appropriately cast after execution of the query.

        Parameters:
            host (string): The data source configured in DataVirtuality. e.g.: "AWB"
            raw_sql (string): The SQL to execute directly on the data source. e.g.: "SELECT * FROM my_table;"

        Returns:
            sql (string)
        """
        raw_sql = raw_sql.replace("'", "''")
        sql = """
            SELECT x.* 
            FROM (call {}.native('{}')) w,
            ARRAYTABLE(w.tuple COLUMNS "val1" string, "val2" string, "val3" string, "val4" string, "val5" string, "val6" string, "val7" string, "val8" string, "val9" string, "val10" string, "val11" string, "val12" string, "val13" string, "val14" string, "val15" string, "val16" string, "val17" string, "val18" string, "val19" string, "val20" string) as x;;
            """.format(host, raw_sql)
        return sql

    def filterNoneFromResult(self, r):
        """
        Remove redundant columns from the result set. This is necessary when the result structure is not derived from an object and is defaulted to 20 string columns.

        Parameters:
            r Array(Array()): The raw result set from the query execution on the server

        Returns:
            result Array(Array())
        """

        result = []
        for i in r:
            record = []
            for m in i:
                if m is not None:
                    val = self.castByType(m)
                    record.append(val)
            result.append(record)
        return result

    def castByType(self, rawValue):
        """
        Best-effort casting of the values based on some simple numeric checks.

        Parameters:
            rawValue (string): The raw value from the query result which is to be recast
        
        Returns:
            val (float|int|string)
        """

        # Check for integer
        try:
            val = int(rawValue)
            return val
        except:
            pass
        
        # Check for float
        try:
            val = float(rawValue)
            return val    
        except:
            pass
        
        # Default to string
        return rawValue
            

    def proxyFromObj(self, host, raw_sql, responseType):
        """
        Proxy an SQL query through the V-DAL with the result structure derived from the responseType object
        
        Parameters:
            host (string): The data source configured in DataVirtuality. e.g.: "AWB"
            raw_sql (string): The SQL to execute directly on the data source. e.g.: "SELECT * FROM my_table;"
            responseType (Object): An object representing the structure of the returned result set

        Returns:
            result Array(responseType)
        """

        sql = self.prepareNativeProcedureCallFromObj(host, raw_sql, responseType)
        return self.query(sql)
    
    def prepareNativeProcedureCallFromObj(self, host, raw_sql, responseType):
        """
        Wrap the SQL to be proxied in the .native() DataVirtuality function.

        Parameters:
            host (string): The data source configured in DataVirtuality. e.g.: "AWB"
            raw_sql (string): The SQL to execute directly on the data source. e.g.: "SELECT * FROM my_table;"
            responseType (Object): An object representing the structure of the returned result set

        Returns:
            sql (string)
        """

        columns = self.getColumnsFromResponseType(responseType)
        raw_sql = raw_sql.replace("'", "''")
        sql = """
            SELECT x.* 
            FROM (call {}.native('{}')) w,
            ARRAYTABLE(w.tuple COLUMNS {}) as x;;
            """.format(host, raw_sql, columns)
        return sql

    def getColumnsFromResponseType(self, responseType):
        columns = []
        for i in inspect.getmembers(responseType):
            if not i[0].startswith('_'):
                if not inspect.ismethod(i[1]):
                    column_name = i[0]
                    column_type = i[1]
                    columns.append("\"{}\" {}".format(column_name, self.getTypeString(column_type)))
        separator = ", "
        return separator.join(columns)
    
    def getTypeString(self, t):
        if isinstance(t, str):
            return "string"
        elif isinstance(t, bool):
            return "bool"
        elif isinstance(t, float):
            return "double"
        elif isinstance(t, int):
            return "int"
        elif isinstance(t, datetime.datetime):
            return "string"
        else:
            return "string"