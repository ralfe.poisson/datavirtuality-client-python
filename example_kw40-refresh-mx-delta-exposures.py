from lib.vdal_client import VDALclient

# Configuration
host = "10.29.158.116"
port = "31000"
database = "datavirtuality"
username = "dv_test"
password = "dv_test"
driver_class = "com.datavirtuality.dv.jdbc.Driver"
driver_file = "D:\datavirtuality\jdbc\datavirtuality-jdbc.jar"

# Instantiate client
client = VDALclient(host, port, username, password, driver_file)

# Prepare SQL
sql = """
            SELECT l_closingdate,
                 l_datekey,
                 REPLACE (m_curve_nam, 'CO2 EUA STOCK', 'CO2 EUA'),
                 TRIM (m_date),
                 TRUNC (m_dated, 'MON'),
                 TRIM (m_index_pro),
                 TRIM (m_index_loc),
                 NVL (TRIM (m_index_sub), 'NONE'),
                 TRIM (m_portfolio),
                 TRIM (m_strategy0),
                 TRIM (m_unit),
                 MARGINING,
                 SUM (m_delta)
            FROM (SELECT l_closingdate                    AS closingdate,
                         l_datekey                        AS m_datekey,
                         CASE
                             WHEN SUBSTR (e.M_VINTAGE, 1, 2) = '20'
                             THEN
                                    TRIM (
                                        SUBSTR (
                                            e.m_curve_nam,
                                            1,
                                              INSTR (e.m_curve_nam || ':', ':')
                                            - 1))
                                 || ' '
                                 || TRIM (e.M_VINTAGE)
                             ELSE
                                 TRIM (
                                     SUBSTR (
                                         e.m_curve_nam,
                                         1,
                                         INSTR (e.m_curve_nam || ':', ':') - 1))
                         END                              AS m_curve_nam,
                         e.m_date,
                         e.m_dated,
                         e.m_index_pro,
                         e.m_index_loc,
                         e.m_index_sub,
                         e.m_portfolio,
                         e.m_strategy0,
                         e.m_unit,
                         e.m_delta,
                         e.m_trade_num,
                         DECODE (m.NB, NULL, 'N', 'Y')    AS MARGINING,
                         m.NB
                    FROM mx_datamart.alpiq_delta_expo_rep@mx_datamart.world e
                         LEFT JOIN MARKETRISK.MX_MARGINING_DEALS m
                             ON     m.NB = e.m_trade_num
                                AND m.CLOSINGDATE = l_closingdate
                   WHERE     e.m_ref_data = l_ref_data
                         AND e.m_dated >= l_closingdate
                         AND NOT TRIM (
                                     SUBSTR (
                                         e.m_curve_nam,
                                         1,
                                         INSTR (e.m_curve_nam || ':', ':') - 1)) =
                                 'CERT CV')
        GROUP BY REPLACE (m_curve_nam, 'CO2 EUA STOCK', 'CO2 EUA'),
                 TRIM (m_date),
                 TRUNC (m_dated, 'MON'),
                 TRIM (m_index_pro),
                 TRIM (m_index_loc),
                 NVL (TRIM (m_index_sub), 'NONE'),
                 TRIM (m_portfolio),
                 TRIM (m_strategy0),
                 TRIM (m_unit),
                 MARGINING;"""

# Get Data
results = client.proxy("MX01", sql)
print(results)