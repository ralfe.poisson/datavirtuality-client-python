# DataVirtuality Client Python

A Python Client for Data Virtuality to assist development teams with making use of the V-DAL.

## Usage

There are three ways in which the client can be used to execute SQL queries on the V-DAL.

1. Direct SQL using DataVirtuality SQL Dialect

```(python)
from lib.vdal_client import VDALclient

client = VDALclient(host, port, username, password, driver_file)
sql = "SELECT * FROM MySource.Table;;"
result = client.query(sql)
print(result)
```

2. Proxy the SQL query through the V-DAL to run on the source system with return class defined

```(python)
from lib.vdal_client import VDALclient

client = VDALclient(host, port, username, password, driver_file)
class myObj:
    someVal = "This is a string"
    otherVal = 10.2
    byInt = 3
sql = "SELECT * FROM table;"
result = client.proxyFromObj("MySource", sql, myObj)
print(result)
```

3. Proxy the SQL query through the V-DAL to run on the source system without specifying return class

```(python)
from lib.vdal_client import VDALclient

client = VDALclient(host, port, username, password, driver_file)
sql = "SELECT * FROM Table;;"
result = client.proxy("MySource", sql)
print(result)
```
